import Component from './components/Component.js';
import Img from './components/Img.js';

//const title = new Component('h1', null, ['La', ' gucci ', 'carte']);
//document.querySelector('.pageTitle').innerHTML = title.render();

const c = new Component('article', { name: 'class', value: 'pizzaThumbnail' }, [
	new Img(
		'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	),
	'Regina',
]);
console.log(c.render());
document.querySelector('.pageContent').innerHTML = c.render();
