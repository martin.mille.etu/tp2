class Component {
	tagName;
	children;
	attribute;
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
	}

	render() {
		let res;
		if (this.attribute != null) {
			res = `< ${this.renderAttribute()} />  `;
		}
		if (this.children != null || this.children != undefined) {
			res += `<${this.tagName}> ${this.renderChildren()}  </${this.tagName}>`;
		} else {
			return '<' + this.tagName + '/>';
		}
		return res;
	}

	renderAttribute() {
		return `img src="${this.attribute.value}"`;
	}

	renderChildren() {
		let path = '';
		if (this.children instanceof Component) {
			return this.children.render();
		} else if (this.children instanceof Array) {
			for (let i = 0; i < this.children.length; i++) {
				path += this.children[i];
			}
		} else {
			path = `${this.children} `;
		}
		return path;
	}
}
export default Component;
