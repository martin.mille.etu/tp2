import Component from './Component.js';
class Img extends Component {
	constructor(valeur) {
		super('img', { name: 'src', value: valeur }, null);
	}
}
export default Img;
